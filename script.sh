#!/usr/bin/env bash

set -e

echo-run() {
    echo "===== ===== $1"
    echo "$($1)"
    echo
}

declare MYHOSTNAME="$(hostname)"

echo-run "hostname"
echo-run "netstat -antup"
echo-run "pwd"
echo-run "ls -al --color=auto ."

# This does not work: "failed to connect to <hostname> port 80: Connection refused"
echo "curl -i http://${MYHOSTNAME}/"
curl -i http://${MYHOSTNAME}/

# As far as I understand this an "inception" problem: in which world am I ?
# MYHOSTNAME is the hostname of the Docker container (seen from inside the container),
# but from a Runner point of view, how is it mapped: which IP ? which port ?
# and how to get these data ?